public class NeptunUser {

    //Privát adattagok létrehozása
    private int id;
    private String neptunid;
    private String token;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNeptunid() {
        return neptunid;
    }

    public void setNeptunid(String neptunid) {
        this.neptunid = neptunid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "NeptunUser{" +
                "id=" + id +
                ", neptunid='" + neptunid + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}
